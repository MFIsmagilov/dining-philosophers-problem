﻿#include "stdafx.h"
#include <windows.h>
#include <stdio.h>
#include <locale>

HANDLE ghSemaphore;
HANDLE *hThreadArray;
HANDLE *ghMutex;

DWORD WINAPI function_thread(LPVOID lpParam);

struct Info{
	int number_round;
	int number_philosopher;
	int index_for_mutex;
};

int _tmain(int argc, _TCHAR* argv[])
{
	setlocale(0, "Russian");
	int N = 5;
	int number_round = 2;

	hThreadArray = new HANDLE[N];
	ghMutex = new HANDLE[N];
	ghSemaphore = CreateSemaphore( 
        NULL,            
        N - 1,   
        N - 1, 
        NULL);

	for(int i = 0 ; i < N; i++)
	{
		ghMutex[i] = CreateMutex(NULL,FALSE,NULL);
	}

	for(int i = 0; i <number_round; i++)
	{
		for(int i = 0; i < N; i++)
		{
			Info *info = new Info();
			info->number_philosopher = i + 1;
			info->index_for_mutex = i;
			hThreadArray[i] = CreateThread(0,0, function_thread, (LPVOID)info,0,0);
		}
	}

	WaitForMultipleObjects(N, hThreadArray, TRUE, INFINITE);
	for(int i = 0; i < N; i++)
	{
		CloseHandle(hThreadArray[i]);
	}
	delete [] hThreadArray;

	for(int i = 0; i < N; i++)
	{
		CloseHandle(ghMutex[i]);
	}
	delete [] ghMutex;

	return 0;
}

DWORD WINAPI function_thread(LPVOID lpParam){

	Info *info = (Info*)lpParam;

	printf(" %d философ размышляет \n",info->number_philosopher);
	Sleep(rand()%5 + 1);

	WaitForSingleObject(ghSemaphore,0);
	WaitForSingleObject(ghMutex[info->index_for_mutex], INFINITE);
	WaitForSingleObject(ghMutex[info->index_for_mutex - 1], INFINITE);
	printf(" %d философ Взял палочки \n", info->number_philosopher);

	printf(" %d Философ Обедает \n", info->number_philosopher);	
	Sleep(rand()%5 + 1);

	ReleaseMutex(ghMutex[info->index_for_mutex]);
	ReleaseMutex(ghMutex[info->index_for_mutex - 1]);
	printf(" %d Философ Положил палочки \n", info->number_philosopher);

	ReleaseSemaphore(ghSemaphore, 1,NULL);
	return 0;
}
